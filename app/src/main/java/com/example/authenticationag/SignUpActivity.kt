package com.example.authenticationag

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity() : AppCompatActivity(){

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init(){
        auth = Firebase.auth
        signUpButton.setOnClickListener {
           signUp()
        }
    }
    private fun signUp(){
        val email: String = emailEditText.text.toString()
        val password: String = passwordEditText.text.toString()
        val repeatPassword: String = repeatPasswordEditText.text.toString()
        val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
        if(email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()){
            if(password == repeatPassword){
                if (email.matches(emailPattern.toRegex())) {
                    progressBar.visibility = View.VISIBLE
                    signUpButton.isClickable = false
                    auth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(this) { task ->
                                progressBar.visibility = View.GONE
                                signUpButton.isClickable = true
                                if (task.isSuccessful) {
                                    d("signUp", "createUserWithEmail:success")
                                    val user = auth.currentUser
                                    Toast.makeText(baseContext, "SignUp is Success!", Toast.LENGTH_SHORT).show()
                                } else {
                                    d("signUp", "createUserWithEmail:failure", task.exception)
                                    Toast.makeText(this, task.exception.toString(), Toast.LENGTH_SHORT).show()
                                    Toast.makeText(baseContext, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show()
                                }
                            }

                } else {
                    Toast.makeText(applicationContext, "Email format is not Correct",
                            Toast.LENGTH_SHORT).show()
                }

            }else{
                Toast.makeText(this, "Passwords does not match!", Toast.LENGTH_SHORT).show()
            }
        }else{
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        }


    }
}
